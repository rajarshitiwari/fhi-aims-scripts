#!/usr/bin/env python3
#
# USAGE:  ./get_relaxation_geometry.py   aims_output_file.out
#

import sys
# import re

if len(sys.argv) != 2:
    print("USAGE:  ./get_relaxation_geometry.py   aims_output_file.out")
    exit()
else:
    AIMS = sys.argv[1]
#

# AIMS = "aims.out"


def get_relaxation_geometries(filename):
    """
    From aims output file, extract the sequence of geometries,
    along with their energy
    input:(str) filename
    """
    with open(filename) as f:
        txt = f.read()
    #
    #
    txt_lines = txt.splitlines()
    num_atoms = [ll for ll in txt_lines if "Number of atoms" in ll][0]
    num_atoms = int(num_atoms.split()[-1])
    # print("Number of atoms: %d" % num_atoms)
    #
    # look for the original geometry with constraints
    l_print = False
    geo0 = []
    for ll in txt_lines:
        if "Completed first pass over input file control.in" in ll:
            l_print = True
        #
        if "Completed first pass over input file geometry.in" in ll:
            l_print = False
        #
        if l_print:
            geo0.append(ll)
        #
    #
    #
    ss = ["lattice_vector", "atom", "constrain"]
    geo0 = [ll for ll in geo0 if any([i in ll for i in ss])]
    #
    geo0 = [ll.lstrip() for ll in geo0]  # strip leading white-space
    #
    atom_locations = [geo0.index(j) for j in [i for i in geo0 if 'atom' in i]]
    # end original geometry
    #
    l_constrain = any(['constrain' in i for i in geo0])
    #
    # relaxed geometries each step
    #
    l_print = False
    content = []
    for ll in txt_lines:
        if "Relaxation step number" in ll:  # start of the relevant text
            l_print = True
        #
        # if "Writing the current" in ll:  # end of the relevant text
        # the above line was wrong. Not sure how it worked for some cases.
        if "Begin self-consistency loop" in ll:  # end of the relevant text
            l_print = False
        #
        if l_print:
            content.append(ll)
        #
    #
    ss = ["lattice_vector", "atom"]  # text to include pattern
    content = [ll for ll in content if any([i in ll for i in ss])]
    ss = ["Updated", "atom_frac"]   # text to exclude pattern.
    # We choose atom, than atom_frac, as that is printed using 'ase'
    content = [ll for ll in content if all([i not in ll for i in ss])]
    #
    content = [ll.lstrip() for ll in content] # strip leading white-space
    #
    num_line = num_atoms + 3
    num_geometry = int(len(content) / num_line)
    geometries = [content[int(ic * num_line):int((ic + 1) * num_line)] for ic in range(num_geometry)]
    #
    # end relaxed geometries

    # insert constraints to each step
    geometries_c = []
    for gi in geometries:
        gic = gi[:3]
        cc = 3
        for ii in range(3, len(geo0)):
            if ii in atom_locations:
                gic.append(gi[cc])
                cc += 1
            else:
                gic.append(geo0[ii])
            #
        geometries_c.append(gic)
    #
    #
    final_traj = "#number_0" + "\n"
    #
    final_traj += '\n'.join(geo0)
    #
    for ii, item in enumerate(geometries_c):
        final_traj += "\n" + "#number_%d" % (ii + 1) + "\n"
        final_traj += '\n'.join(item)
    #
    #
    print(final_traj)
    # ss = ["Updated", "atom_frac"]
    # content = [ll for ll in content if all([i not in ll for i in ss])]
#


a = get_relaxation_geometries(AIMS)

print(a)

"""
num_line = num_atoms + 3
num_geometry = int(len(content) / num_line)
geometries = [content[int(ic * num_line):int((ic + 1) * num_line)] for ic in range(num_geometry)]

for ii, item in enumerate(geometries):
    print("# number %d" % ii)
    print('\n'.join(item))
#

"""
