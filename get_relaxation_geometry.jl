#!/usr/bin/env julia
#
# USAGE:  ./get_relaxation_geometry.jl   aims_output_file.out
#


if size(ARGS, 1) != 1
    println("USAGE:  ./get_relaxation_geometry.jl   aims_output_file.out")
    @show ARGS
    exit(1)
else
    AIMS = ARGS[1]
end

# AIMS = "aims.out"

txt = open(AIMS) do f
    read(f, String)
end

@show txt
